# Simple Gulp template

## Install dependencies

1. Install gulp global `npm install -g gulp`.
2. Install dependencies `npm i`.

## Command

1. Run `gulp watch` to watch file change and auto reload browser.
2. Run `gulp buid` to build.

## Note
1. browserSync serve at port `3000`, feel free to change it in `gulpfile.js`.
2. `scss` and `html` named start with `_` will exclude form buid command (not create file in dist folder).
3. All javascript file will be combined to 1 file named `app.js` located at `dist/scripts/app.js`.
